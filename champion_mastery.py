#! /usr/bin/python3

from main import lolapi
import datetime

api = lolapi()
riotapi = api.get_api()
summoner_name = input ("Please enter your summoner name (%s)>> " %(api.region))
summoner = riotapi.get_summoner_by_name(summoner_name)
champ_masteries = summoner.champion_masteries().values()

## print decending
print ("Here are your 10 lowest non-zero champion masteries")
points = [(x.points,x) for x in champ_masteries]
desc_sorted = sorted(points, key=lambda x:x[0])
for entry in desc_sorted[0:10]:
    print ("\tYou have {} on {}".format(entry[0], entry[1].champion.name))

## print last played
print ("Here are 10 champions you haven't played in a while")
dates = [(x.last_played, x) for x in champ_masteries]
desc_sorted = sorted(dates, key=lambda x:x[0])
now = datetime.datetime.now()
for entry in desc_sorted[0:10]:
    print ("\tYou Last played {champ} on {date} which was {daysago} days ago"
    .format(
        champ=entry[1].champion.name,
        date=entry[0].strftime("%d/%m/%y at %H:%M:%S"),
        daysago=((now-entry[0]).days))
    )
