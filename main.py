import cassiopeia.riotapi as riotapi
from sys import exit
import os

class lolapi():
    region = ["NA","OCE","EUW"][1] #change your region here.
    def __init__(self):
        if not os.path.exists("api-key.txt"):
            print ("Please create a File called api-key.txt with your api key.")
            exit(-1);
        apiFile = open ("api-key.txt", 'r')
        self.api_key = apiFile.read().replace("\n","")
        riotapi
        riotapi.set_region(lolapi.region)
        riotapi.set_api_key(self.api_key)

    def test (self):
        summoner = riotapi.get_summoner_by_name("doublelift")
        print("%s is a level %d summoner on the %s server." % (summoner.name, summoner.level,lolapi.region) )


    def get_api (self):
        return riotapi
